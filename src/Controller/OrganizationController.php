<?php

declare(strict_types=1);


namespace App\Controller;

use App\Entity\Organization;
use App\Services\OrganizationService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class OrganizationController extends AbstractController
{
    /**
     * @Route("/api/organizations/siret/{siret}",name="getBySiretOrganization", methods={"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function getBySiret(Request $request, EntityManagerInterface $em): Response
    {
        $siret = $request->get('siret');

        /** @var Organization $organization */
        $organizationService = new OrganizationService($em);
        $organization = $organizationService->getOrganizationBySiret($siret);

        if(!$organization){
            throw new NotFoundHttpException('Siret not found');
        }

        $encoders = [new JsonEncoder()];
        $normalizers = [new ObjectNormalizer()];
        $serializer = new Serializer($normalizers, $encoders);
        $jsonContent = $serializer->serialize($organization, 'json');

        return new Response($jsonContent,200);
    }

    /**
     * @Route("/api/organizations/siret_exists/{siret}",name="isSiretOrganizationExists", methods={"GET"})
     * @param Request $request
     * @param EntityManagerInterface $em
     * @return Response
     */
    public function getSiretValidation(Request $request, EntityManagerInterface $em):Response{

        $siret = $request->get('siret');

        /** @var Organization $organization */
        $organizationService = new OrganizationService($em);
        $siretExists = $organizationService->checkIsSiretExists($siret);

        return new Response(json_encode([ 'response' => $siretExists ]),200);

    }

}