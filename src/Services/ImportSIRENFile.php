<?php

declare(strict_types=1);

namespace App\Services;

use App\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Exception\RequestException;
use Psr\Log\LoggerInterface;
use GuzzleHttp\Client;

class ImportSIRENFile
{
    protected const SEPARATOR = ';';
    protected const FILENAME = 'sirets.zip';
    protected const SIREN_FIELD = 0;
    protected const ORGANIZATION_NAME_FIELD = 36;

    /** @var string */
    protected $filePath;

    /** @var string */
    protected $endpoint;

    /** @var string */
    protected $importFileDirectory;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var LoggerInterface */
    protected $logger;

    /** @var int */
    protected $row = 0;

    public function __construct(string $endpoint, string $importFileDirectory, EntityManagerInterface $em, LoggerInterface $logger)
    {
        $this->endpoint = $endpoint;
        $this->importFileDirectory = $importFileDirectory;
        $this->em = $em;
        $this->logger = $logger;
    }

    // import du fichier CSV
    public function import():void{
        try{
            $this->download();

            if(!file_exists($this->filePath)){
                throw new \UnexpectedValueException('SIREN file does not exists');
            }
            $this->saveCSVFile();
            echo "Import réussi";
        }catch (\Throwable $t){
            $this->logger->error($t->getMessage());
        }
    }

    // Téléchargement du fichier de MAJ
    protected function download():void{
        try{
            if(!$this->endpoint){
                throw new \UnexpectedValueException('Bad endpoint value');
            }
            $client = new Client(['base_uri' => 'http://files.data.gouv.fr/']);
            $response = $client->request('GET', $this->endpoint, ['sink' => $this->importFileDirectory.self::FILENAME]);

            if($response->getStatusCode() === 200){
                $zip = new \ZipArchive();
                if ($zip->open($this->importFileDirectory.self::FILENAME) === TRUE) {
                    $zip->extractTo($this->importFileDirectory);
                    $this->filePath = $this->importFileDirectory.$zip->getNameIndex(0); // on récupère le nom de l'unique fichier dans le zip
                    $zip->close();
                } else {
                    throw new \Exception('Impossible to unzip file "'.$this->importFileDirectory.self::FILENAME.'"');
                }
            }
        }catch (RequestException $requestException){
            $this->logger->error($requestException->getMessage());
        }catch (\Throwable $t){
            $this->logger->error($t->getMessage());
        }
    }

    // Lecture et sauvegarde des colonnes du fichier CSV
    protected function saveCSVFile():void{
        if (($handle = fopen($this->filePath, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, self::SEPARATOR)) !== FALSE) {
                $this->row++;

                // on échappe la 1ere ligne qui contient l'en-tête
                if($this->row == 1){
                    continue;
                }

                if($this->rowValidation($data)){
                    $this->saveRow($data);

                    if($this->row % 500 === 0){
                        $this->em->flush();
                    }
                }
            }
            fclose($handle);
            $this->em->flush();
        }
    }

    /**
     * Sauvegarde de l'objet "Organization"
     * @param array $data
     */
    protected function saveRow(array $data):void{
        $name = utf8_encode($data[self::ORGANIZATION_NAME_FIELD]);
        $siret = utf8_encode($data[self::SIREN_FIELD]);

        try{
            /** @var Organization $organization */
            $organization = $this->em->getRepository('App:Organization')->findOneBy(['siret' => $siret]);

            if(!$organization){
                $organization = new Organization();
            }

            $organization->setName($name)
                ->setSiret($siret);

            // sauvegarde de l'organization
            $this->em->persist($organization);
        }catch (\Throwable $t){
            $this->logger->error($t->getMessage());
        }
    }

    /**
     * @param array $data
     * @return bool
     */
    protected function rowValidation(array $data):bool{
        if(isset($data[self::SIREN_FIELD]) && isset($data[self::ORGANIZATION_NAME_FIELD])
            && $data[self::SIREN_FIELD] !== '' && $data[self::ORGANIZATION_NAME_FIELD] !== ''){
            return true;
        }
        return false;
    }
}