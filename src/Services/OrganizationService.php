<?php

declare(strict_types=1);


namespace App\Services;


use App\Entity\Organization;
use Doctrine\ORM\EntityManagerInterface;

class OrganizationService
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $em;


    public function __construct(EntityManagerInterface $em){
        $this->em = $em;
    }

    /**
     * @param $siret
     * @return Organization|null
     */
    public function getOrganizationBySiret($siret):?Organization{
        return $this->em->getRepository('App:Organization')->findOneBy(['siret' => $siret]);
    }

    /**
     * @param $siret
     * @return bool
     */
    public function checkIsSiretExists($siret):bool{
        $organization = $this->em->getRepository('App:Organization')->findOneBy(['siret' => $siret]);
        return $organization ? true : false;
    }
}