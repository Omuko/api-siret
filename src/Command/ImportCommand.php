<?php

declare(strict_types=1);

namespace App\Command;

use App\Services\ImportSIRENFile;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBag;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class ImportCommand extends Command
{
    /** @var string */
    protected static $defaultName = 'app:import-file';

    /** @var ParameterBag */
    protected $parameterBag;

    /** @var EntityManagerInterface */
    protected $em;

    /** @var LoggerInterface */
    protected $logger;

    /**
     * ImportCommand constructor.
     * @param ParameterBag $pg
     * @param string|null $name
     */
    public function __construct(ParameterBagInterface $pg, EntityManagerInterface $em, LoggerInterface $logger, string $name = null)
    {
        $this->parameterBag = $pg;
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function configure(): void
    {
        $this->setDescription('Import SIREN file')
            ->addOption(
                'endpoint',
                null,
                InputOption::VALUE_REQUIRED
            );
    }

    /**
     * Execution de la commande d'import du fichier de MAJ des SIRETS
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $filename = $input->getOption('endpoint');
        $importFilesDirectory = $this->parameterBag->get('kernel.project_dir').'/'.$this->parameterBag->get('importFilesDirectory');

        (new ImportSIRENFile($filename, $importFilesDirectory, $this->em, $this->logger))->import();

        return Command::SUCCESS;
    }
}