# API de recherche de SIREN

## Installation
Renseigner les informations de connexion à la base dans le fichier .env
L'utilisation de l'API nécessite pour le téléchargement des fichiers l'activation de l'extension php "ZipArchive".

Installation des composants de base :
<pre>
$ composer install
</pre>

### Création de la base
<pre>
$ bin/console doctrine:database:create
</pre>

### Jouer les migrations
Lancer le script d'exécution des migrations 
<pre>
$ php bin/console doctrine:migrations:migrate
</pre>

### Import d'un fichier de MAJ
<pre>
$ php bin/console app:import-file --endpoint=sirene/sirene_2018088_E_Q.zip
</pre>

## Appels à l'API
### Recherche de l'existence d'un siret
<pre>
$ curl -X 'GET' 'http://localhost/api/organizations/siret_exists/{siret}' -H 'accept: application/json'
</pre>

### Recherche d'informations sur une entreprise grâce à son SIRET'
<pre>
$ curl -X 'GET' 'http://localhost/api/organizations/siret/{siret}' -H 'accept: application/json'
</pre>

Si aucun siret n'est trouvé, l'API renverra une erreur 404.
